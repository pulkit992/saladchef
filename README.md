## Salad Chef ##

### Player Controller ###
- Created player's basic movement using wasd keys for left player and arrow keys for right player also differentiate two players using enums, added UI on both players which always faces towards camera so that user can easily see, and working on vegetables pick up and drop functions for player.
- Now player is able to pick up vegetables from vegetable table and can place that on plate near to chopper, player can drop vegetable into chopper but chopper functioning is still remaining and also drop into dustbin.
- Added functioning for player's UI, whenever player picking up or dropping vegetable its showing in UI and also has warning/info text which shows up whenever player can't drop or pick up anything.
- Player can pick combo/salad and drop them into trash/dustbin.
- Now player can pick up reward and speed up functionality working and also added speed up bar in player's HUD.

### Restaurant Structure ###
- Restaurant basic structure created with table and items on table.
- Whenever player is coming nearby to any item placed on table the portion around that item is highlighting so that user can understand that player is now selecting this item.
- UI added on each vegetable, which contains image and name of that vegetable so that player can understand.
- Chopper functionality done, now player can place vegetables into chopper then chopper chops and player has to wait for that time and player can't move.
- Reward spawning at random place in restaurant.

### Customer Spawn and Controller ###
- Customers are spawning at random time, functionality of customer angry, dissatisfied, reward to player on or before 70% of waiting time, penalized to player when wrong salad serves
- customer tags were missing from the customer prefabs

### Level Controller ###
- Score and timer added for both player. Game over pop up is showing when timer of both players is zero.
- score is deducting when player throws salad into dustbin, when player is penalized by customer or customer is not satisfied.
- Replay functioning is working and top 10 is saving in a text file
- chopper and plate references were missing into level controller class


### UI ###
- Header is added in canvas which shows score and timer for both player and a award icon added for both player which also has counter, counter shows no of award particular player has.
- Help button is added which shows control keys for both players.

Reset score.txt file and added some tool tip to level controller class

