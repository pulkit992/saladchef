﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// salad combinations
/// </summary>
[System.Serializable]
public class SaladCombinations
{
    public List<VegetableType> vegetable;
}

public class CustomerSpawner : MonoBehaviour {

    /// <summary>
    /// list contains different type of customers
    /// </summary>
    public List<GameObject> customers;

    /// <summary>
    /// it is a waiting time for each vegetable
    /// waiting time will multiply with no of
    /// vegetables customer need for salad
    /// </summary>
    [Tooltip("Waiting time for customer, it multiplies with the items in the salad to define total customer waiting time")]
    public float waitingTime;

    /// <summary>
    /// different type of salad combinations
    /// </summary>
    public List<SaladCombinations> Salads;

    /// <summary>
    /// customer counter/table
    /// where customer stands
    /// </summary>
    public List<CustomerCounter> customerCounters;

	// Use this for initialization
	void Start () {
        InitializeCustoemrs();
    }

    /// <summary>
    /// initialize custoemrs
    /// </summary>
    public void InitializeCustoemrs ()
    {
        Debug.Log("initializing in custoemr spawner");
        StartCoroutine(StartInitialSpawning());
    }

    IEnumerator StartInitialSpawning ()
    {
        for (int i = 0; i < customerCounters.Count; i++)
        {
            SpawnCustomer();
            float randomTime = Random.Range(1f, 7f);
            yield return new WaitForSeconds(randomTime);         
        }
    }

    public void ResetTable ()
    {
        for (int i = 0; i < customerCounters.Count; i++)
        {
            customerCounters[i].isCounterEmpty = true;
            if (customerCounters[i].customerController != null)
                Destroy(customerCounters [i].customerController.gameObject);
        }
    }

    public void SpawnNewCustomer ()
    {
        StartCoroutine(SpawningCustomer ());
    }

    /// <summary>
    /// spawn customer at random delay
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawningCustomer ()
    {
        float randomTime = Random.Range(1f, 4f);
        yield return new WaitForSeconds(randomTime);
        if (!LevelController.isGameOver)
            SpawnCustomer();
    }
	
    /// <summary>
    /// removes all customers
    /// </summary>
    public void RemoveAllCustomers ()
    {
        GameObject[] objs;
        objs = GameObject.FindGameObjectsWithTag("CustomerTag");
        foreach (GameObject obj in objs)
        {
            Destroy(obj);
        }
    }

	public void SpawnCustomer ()
    {
        if (LevelController.isGameOver)
            return;

        // get random salad combination
        List<VegetableType> type = new List<VegetableType>();
        type = Salads[Random.Range (0, Salads.Count)].vegetable;

        // get random customer and instantiate at spawn position
        GameObject customer = Instantiate(customers [Random.Range (0, customers.Count)]) as GameObject;
        customer.transform.position = transform.position;
        customer.transform.Rotate (0f, 270f, 0f);

        // find customer controller instance of instantiated customer
        CustomerController controller = customer.GetComponent<CustomerController>();

        // find out empty table/counter
        int counterIndex = -1;
        for (int i = 0; i < customerCounters.Count; i++)
        {
            if (customerCounters[i].isCounterEmpty)
            {
                counterIndex = i;
                break;
            }
        }

        if (counterIndex == -1)
            return;

        // counter is not empty
        customerCounters[counterIndex].isCounterEmpty = false;
        // move customer towards counter
        controller.MoveCustomerTowardsTable(customerCounters [counterIndex], type, waitingTime);
    }
}
