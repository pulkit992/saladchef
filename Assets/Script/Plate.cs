﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plate : MonoBehaviour {

    /// <summary>
    /// vegetable type
    /// </summary>
    public VegetableType vegType;

    /// <summary>
    /// canvas object for plate
    /// </summary>
    public GameObject canvasObj;

    /// <summary>
    /// image representation for vegetable
    /// which plate has
    /// </summary>
    public Image vegetableImage;

    /// <summary>
    /// name representation for vegetable
    /// which plate has
    /// </summary>
    public Text vegetableName;

    /// <summary>
    /// instance of level controller
    /// </summary>
    LevelController levelController;

    void Start()
    {
        // veg type should be empty at the starting of game
        // because plate is empty 
        vegType = VegetableType.Empty;

        // no canvas is showing for plate at start
        canvasObj.SetActive(false);

        // finding level controller script for level controller instance
        levelController = GameObject.Find("LevelController").GetComponent<LevelController>();
    }

    /// <summary>
    /// checking if plate has any vegetable
    /// </summary>
    /// <returns></returns>
    public bool HasAnyVegetable()
    {
        if (vegType != VegetableType.Empty)
            return true;
        else
            return false;
    }

    /// <summary>
    /// contains vegetable drop by player
    /// </summary>
    /// <param name="salad"></param>
    public void CarryVegetable(Salad salad)
    {
        vegType = salad.type;
        canvasObj.SetActive(true);
        int index = levelController.GetVegeTableIndex(salad.type);
        vegetableImage.sprite = levelController.vegetableData[index].vegetableImage;
        vegetableName.text = levelController.vegetableData[index].vegetableName;
            
        Debug.Log("Plate has " + vegType);
    }

    public void ResetPlate ()
    {
        vegType = VegetableType.Empty;
        canvasObj.SetActive(false);
    }

    /// <summary>
    /// remove vegetable from plate
    /// </summary>
    /// <returns></returns>
    public VegetableType RemoveVegetable ()
    {
        VegetableType temp = vegType;
        vegType = VegetableType.Empty;
        canvasObj.SetActive(false);
        Debug.Log("Now plate is empty");
        return temp;
    }
}
