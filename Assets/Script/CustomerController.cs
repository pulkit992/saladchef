﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerController : MonoBehaviour {
    /// <summary>
    /// contains the list of vegetables
    /// which customer need in his/her
    /// salad
    /// </summary>
    public List<VegetableType> vegetables;

    /// <summary>
    /// customer's canvas
    /// </summary>
    public GameObject canvasObj;

    /// <summary>
    /// customer's info text
    /// </summary>
    public Text infoText;

    /// <summary>
    /// customer's waiting bar
    /// </summary>
    public Image customerWaitingBar;

    /// <summary>
    /// contains all vegetables images
    /// </summary>
    Transform customerDemand;

    /// <summary>
    /// prefab for vegetable image
    /// </summary>
    public GameObject vegetableImg;

    /// <summary>
    /// instance for level controller
    /// </summary>
    LevelController levelController;

    /// <summary>
    /// customer's waiting time
    /// </summary>
    [HideInInspector]
    public float customerWaitingTime;

    /// <summary>
    /// customer's current time
    /// remaining for wait
    /// </summary>
    float currentTime;

    /// <summary>
    /// ratio of current time and 
    /// total waiting time
    /// </summary>
    [HideInInspector]
    public float timerRatio;

    /// <summary>
    /// waiting timer start
    /// </summary>
    [HideInInspector]
    public bool startTimer = false;

    /// <summary>
    /// customer counter
    /// </summary>
    CustomerCounter customerCounter;

    /// <summary>
    /// position of customer table
    /// </summary>
    Vector3 tablePos;

    /// <summary>
    /// starting position of customer
    /// </summary>
    Vector3 startPos;

    /// <summary>
    /// moving towards table
    /// </summary>
    bool getInRestaurant = false;

    /// <summary>
    /// getting out from restaurant
    /// </summary>
    bool leaveRestaurant = false;

    /// <summary>
    /// moving speed of customer
    /// </summary>
    public float customerMoveSpeed;

    /// <summary>
    /// main camera in scene
    /// </summary>
    Camera cam;

    /// <summary>
    /// list of players who serves wrong salad
    /// </summary>
    public List<PlayerController> playerControllers;

    /// <summary>
    /// this is time scale which we can use
    /// to decrease time more fast
    /// </summary>
    public float waitingTimeScale = 1f;

    private void Start()
    {
        // HUD should be disable at starting
        canvasObj.SetActive(false);

        // start position of customer
        startPos = transform.position;

        cam = Camera.main;
    }

    /// <summary>
    /// leave the restaurant after 1 or 2 sec
    /// </summary>
    /// <returns></returns>
    IEnumerator Leaving ()
    {
        yield return new WaitForSeconds(1.2f);
        canvasObj.SetActive(false);
        leaveRestaurant = true;
    }

    public void LeavingRestaurant ()
    {
        StartCoroutine(Leaving ());
    }

    /// <summary>
    /// both custoemr penalized
    /// </summary>
    /// <param name="deductionRateMultiplayer"></param>
    void BothCustomerPenalized (int deductionRateMultiplayer)
    {
        levelController.leftPlayerScore -= (levelController.scoreDeductionRate * deductionRateMultiplayer);
        HeaderUI.instance.leftPlayerScore.text = "" + levelController.leftPlayerScore;

        levelController.rightPlayerScore -= (levelController.scoreDeductionRate * deductionRateMultiplayer);
        HeaderUI.instance.rightPlayerScore.text = "" + levelController.rightPlayerScore;
    }

    private void Update()
    {
        if (startTimer)
        {
            currentTime -= Time.deltaTime * waitingTimeScale;
            if (currentTime <= 0)
            {
                Debug.Log("time over");
                startTimer = false;

                // deduction rate multiplayer make 2
                // in case of angry customer
                int deductionRateMultiplayer = 1;
                bool isCustomerAngry = false;

                infoText.text = "Dissatisfied";
                customerCounter.isCounterEmpty = true;

                if (waitingTimeScale == 2)
                {
                    // customer is angry and will penalize to player
                    // who serves wrong salad to him
                    Debug.Log("angry customer - penalize " + playerControllers.Count);
                    infoText.text = "Angry n dissatisfied!";
                    deductionRateMultiplayer = 2;
                    isCustomerAngry = true;
                }

                // score duduction
                if (isCustomerAngry)
                {
                    if (playerControllers.Count == 1)
                    {
                        PlayerUserController userController = playerControllers[0].GetComponent<PlayerUserController>();
                        if (userController.playerType == PlayerType.LeftPlayer)
                        {
                            levelController.leftPlayerScore -= (levelController.scoreDeductionRate * deductionRateMultiplayer);
                            HeaderUI.instance.leftPlayerScore.text = "" + levelController.leftPlayerScore;
                        }
                        else if (userController.playerType == PlayerType.RightPlayer)
                        {
                            levelController.rightPlayerScore -= (levelController.scoreDeductionRate * deductionRateMultiplayer);
                            HeaderUI.instance.rightPlayerScore.text = "" + levelController.rightPlayerScore;
                        }
                    }
                    else if (playerControllers.Count == 2)
                    {
                        // both player penalize
                        BothCustomerPenalized(deductionRateMultiplayer);
                    }
                }
                else
                {
                    // customer dissatisfied
                    BothCustomerPenalized(deductionRateMultiplayer);
                }
                StartCoroutine(Leaving ());
            }
            timerRatio = currentTime / customerWaitingTime;
            customerWaitingBar.fillAmount = timerRatio;
        }

        if (getInRestaurant)
        {
            float xPosition = transform.position.x;
            float zPosition = transform.position.z;
            if (xPosition < tablePos.x)
            {
                xPosition += Time.deltaTime * customerMoveSpeed;
            }
            else if (zPosition > (tablePos.z + 1.5f))
            {
                transform.rotation = Quaternion.Euler(0f ,180f, 0f);
                zPosition -= Time.deltaTime * customerMoveSpeed;
            }
            else
            {
                canvasObj.SetActive(true);
                startTimer = true;
                getInRestaurant = false;
                canvasObj.transform.parent.LookAt(cam.transform);
            }
            
            // move on local z axis
            transform.localPosition = new Vector3(xPosition, transform.position.y, zPosition);
        }

        if (leaveRestaurant)
        {
            float xPosition = transform.position.x;
            float zPosition = transform.position.z;
            if (zPosition < startPos.z)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                zPosition += Time.deltaTime * customerMoveSpeed;
            }
            else if (xPosition > startPos.x)
            {
                transform.rotation = Quaternion.Euler(0f, 270f, 0f);
                xPosition -= Time.deltaTime * customerMoveSpeed;
            }
            else
            {
                leaveRestaurant = false;
                customerCounter.isCounterEmpty = true;
                CustomerSpawner spawner = GameObject.Find("CustomerSpawner").GetComponent<CustomerSpawner> ();
                spawner.SpawnNewCustomer ();

                Destroy(gameObject);
            }

            // move on local z axis
            transform.localPosition = new Vector3(xPosition, transform.position.y, zPosition);
        }
    }

    /// <summary>
    /// setting up customer UI
    /// </summary>
    void SetCustomerUI ()
    {
        // waiting timer of customer for salad
        currentTime = customerWaitingTime;

        levelController = GameObject.Find("LevelController").GetComponent<LevelController>();
        customerDemand = canvasObj.transform.Find("CustomerDemand").transform;

        // instantiate all vegetables in customer's canvas
        for (int i = 0; i < vegetables.Count; i++)
        {
            Debug.Log("get vegetable for HUD");
            GameObject obj = Instantiate(vegetableImg) as GameObject;
            obj.transform.SetParent(customerDemand, false);
            int index = levelController.GetVegeTableIndex(vegetables[i]);
            obj.GetComponent<Image>().sprite = levelController.vegetableData[index].vegetableImage;
        }
    }

    /// <summary>
    /// move customer towards table
    /// </summary>
    public void MoveCustomerTowardsTable (CustomerCounter counter, List<VegetableType> _vegetables, float waitingTime)
    {
        customerCounter = counter;
        customerCounter.customerController = this;
        vegetables = _vegetables;
        customerWaitingTime = waitingTime * vegetables.Count;

        // set customer HUD
        SetCustomerUI();

        // get table position
        tablePos = counter.transform.position;
        getInRestaurant = true;

        Debug.Log("customer moving");
    }
}
