﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chopper : MonoBehaviour {

    /// <summary>
    /// salad
    /// </summary>
    public List<VegetableType> vegetableType;

    /// <summary>
    /// time for chopping a vegetable
    /// </summary>
    public float choppingTime;

    /// <summary>
    /// HUD for chopper
    /// </summary>
    public GameObject canvasObj;

    /// <summary>
    /// handles images of vegetable
    /// chopper currently have
    /// </summary>
    Transform saladHandler;

    /// <summary>
    /// contains the image of vegetable
    /// </summary>
    public GameObject vegetableImg;

    /// <summary>
    /// shows the current working of chopper
    /// chopping/done/chopper is full
    /// </summary>
    public Text infoText;

    /// <summary>
    /// maximum items chopper can handel 
    /// for one salad combination
    /// </summary>
    public int maxSaladItems;

    /// <summary>
    /// instance of nearby player controller
    /// </summary>
    [HideInInspector]
    public PlayerController playerController;

    /// <summary>
    /// instance of level controller
    /// </summary>
    LevelController levelController;

    private void Start()
    {
        // finding salad handler 
        // which contains all images of 
        // vegetables, which chopper currently have
        saladHandler = canvasObj.transform.Find("SaladHandler").transform;

        // hide canvas at starting
        canvasObj.SetActive(false);

        // instance of level controller
        levelController = GameObject.Find("LevelController").GetComponent<LevelController>();
    }

    /// <summary>
    /// reset chopper
    /// </summary>
    public void ResetChopper ()
    {
        vegetableType.Clear();
        canvasObj.SetActive(false);
        foreach (Transform child in saladHandler)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// checking if chopper has any combination
    /// </summary>
    /// <returns></returns>
    public bool HasAnyCombination()
    {
        if (vegetableType.Count > 0)
            return true;
        else
            return false;
    }

    /// <summary>
    /// clears info text
    /// </summary>
    void ClearInfoText()
    {
        infoText.text = "";
    }

    IEnumerator Chopping ()
    {
        if (playerController != null)
            playerController.canPlayerMove = false;
        infoText.text = "Chopping...";

        yield return new WaitForSeconds(choppingTime);

        infoText.text = "Done!";
        playerController.canPlayerMove = true;
    }

    /// <summary>
    /// drops vegetable to chopper
    /// </summary>
    /// <param name="_salad"></param>
    public void DropVegForChop (VegetableType _vegetable)
    {
        vegetableType.Add(_vegetable);

        if (!canvasObj.activeSelf)
            canvasObj.SetActive(true);

        // instantiate a vegetable image for HUD
        // so that player can understand that
        // which vegetables chopper has
        GameObject obj = Instantiate(vegetableImg) as  GameObject;
        obj.transform.SetParent(saladHandler, false);
        int index = levelController.GetVegeTableIndex(_vegetable);
        obj.GetComponent<Image>().sprite = levelController.vegetableData[index].vegetableImage;

        StartCoroutine(Chopping ());
    }

    /// <summary>
    /// checking if chopper is full or not
    /// </summary>
    /// <returns></returns>
    public bool IsChopperFull ()
    {
        return vegetableType.Count >= maxSaladItems;
    }

    /// <summary>
    /// returns chopped salad
    /// </summary>
    /// <returns></returns>
    public List<VegetableType> ChoppedSalad ()
    {
        List<VegetableType> tempVegList = new List<VegetableType>();
        for (int i = 0; i < vegetableType.Count; i++)
            tempVegList.Add(vegetableType [i]);

        canvasObj.SetActive(false);

        foreach (Transform child in saladHandler)
        {
            GameObject.Destroy(child.gameObject);
        }

        vegetableType.Clear();
        Debug.Log("list count " + tempVegList.Count + "  " + vegetableType.Count);
        return tempVegList;
    }
}
