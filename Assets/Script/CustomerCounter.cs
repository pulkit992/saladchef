﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerCounter : MonoBehaviour {

    /// <summary>
    /// customer controller reference 
    /// </summary>
    public CustomerController customerController;

    /// <summary>
    /// checks if counter empty/filled
    /// </summary>
    public bool isCounterEmpty = true;

    /// <summary>
    /// level controller
    /// </summary>
    LevelController levelController;

    private void Start()
    {
        // instance of level controller
        levelController = GameObject.Find("LevelController").GetComponent<LevelController>();
    }

    /// <summary>
    /// get combination
    /// </summary>
    /// <param name="salad"></param>
    /// <param name="_playercontroller"></param>
    public void GetCombination (Salad salad, PlayerController _playercontroller)
    {
        List<VegetableType> saladList = new List<VegetableType>();
        //saladList = customerController.vegetables;
        for (int i = 0; i < customerController.vegetables.Count; i++)
        {
            saladList.Add(customerController.vegetables [i]);
        }
        bool hasCorrectSalad = false;
        Debug.Log("getCombination" + salad.combination.Count + ", salad list " + saladList.Count);
        if (saladList.Count == salad.combination.Count)
        {
            for (int i = 0; i < salad.combination.Count; i++)
            {
                for (int j = 0; j < saladList.Count; j++)
                {
                    if (salad.combination[i] == saladList[j])
                        saladList.RemoveAt(j);
                }
            }

            if (saladList.Count == 0)
                hasCorrectSalad = true;

            if (hasCorrectSalad)
            {
                customerController.startTimer = false;

                PlayerUserController userController = _playercontroller.GetComponent<PlayerUserController>();
                int pointMultiplayer = salad.combination.Count;

                // adding points to players score
                if (userController.playerType == PlayerType.LeftPlayer)
                {
                    levelController.leftPlayerScore += (levelController.scoreDeductionRate * pointMultiplayer);
                    HeaderUI.instance.leftPlayerScore.text = "" + levelController.leftPlayerScore;
                }
                else if (userController.playerType == PlayerType.RightPlayer)
                {
                    levelController.rightPlayerScore += (levelController.scoreDeductionRate * pointMultiplayer);
                    HeaderUI.instance.rightPlayerScore.text = "" + levelController.rightPlayerScore;
                }

                // checking if player is eligiable for reward or not
                if (customerController.timerRatio >= 0.7f)
                {
                    // give reward to player
                    Debug.Log("Excellient deleviery of salad, give reward to " + _playercontroller);
                    customerController.infoText.text = "Reward!";
                    levelController.SpawnAward(_playercontroller.gameObject.GetComponent<PlayerUserController> ().playerType);
                }
                else
                {
                    Debug.Log("salad delevired");
                    customerController.infoText.text = "Thank you!";


                }
                customerController.LeavingRestaurant();
            }
            else
            {
                Debug.Log("incorrect salad, make customer angry");
                // adding player into list who serves wrong salad to customer
                if (!customerController.playerControllers.Contains(_playercontroller))
                    customerController.playerControllers.Add(_playercontroller);

                // decrease waiting time fast
                customerController.waitingTimeScale = 2f;

                customerController.infoText.text = "Angry!";
            }
        }
        else
        {
            Debug.Log("incorrect salad, make customer angry");
            // adding player into list who serves wrong salad to customer
            if (!customerController.playerControllers.Contains(_playercontroller))
                customerController.playerControllers.Add(_playercontroller);

            // decrease waiting time fast
            customerController.waitingTimeScale = 2f;

            customerController.infoText.text = "Angry!";
        }
    }
}
