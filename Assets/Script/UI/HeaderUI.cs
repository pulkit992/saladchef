﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderUI : MonoBehaviour {

    /// <summary>
    /// instance of header ui
    /// </summary>
    public static HeaderUI instance;

    /// <summary>
    /// left player score
    /// </summary>
    public Text leftPlayerScore;

    /// <summary>
    /// right player score
    /// </summary>
    public Text rightPlayerScore;

    /// <summary>
    /// left player timer
    /// </summary>
    public Text leftPlayerTimer;

    /// <summary>
    /// right player timer
    /// </summary>
    public Text rightPlayerTimer;

    /// <summary>
    /// left player award image
    /// </summary>
    public GameObject leftPlayerAward;

    /// <summary>
    /// right player award image
    /// </summary>
    public GameObject rightPlayerAward;

    /// <summary>
    /// left player award text
    /// </summary>
    public Text leftPlayerAwardText;

    /// <summary>
    /// right player award text
    /// </summary>
    public Text rightPlayerAwardText;

    private void Awake()
    {
        if (instance == null)
        instance = this;
    }

    /// <summary>
    /// updating award for left player
    /// </summary>
    /// <param name="count"></param>
    public void UpdateLeftPlayerAward (int count)
    {
        if (!leftPlayerAward.activeSelf)
            leftPlayerAward.SetActive(true);

        leftPlayerAwardText.text = count + "";
        if (count <= 0)
            leftPlayerAward.SetActive(false);
    }

    /// <summary>
    /// updating award for right player
    /// </summary>
    /// <param name="count"></param>
    public void UpdateRightPlayerAward (int count)
    {
        if (!rightPlayerAward.activeSelf)
            rightPlayerAward.SetActive(true);

        rightPlayerAwardText.text = count + "";
        if (count <= 0)
            rightPlayerAward.SetActive(false);
    }

    /// <summary>
    /// help button
    /// </summary>
    public void OnHelpButton ()
    {
        HelpUI.ShowUI();
    }
}
