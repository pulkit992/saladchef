﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    /// <summary>
    /// serial no
    /// </summary>
    public Text serialNo;

    /// <summary>
    /// player score
    /// </summary>
    public Text scoreText;

    /// <summary>
    /// player name
    /// </summary>
    public Text playerName;

    /// <summary>
    /// player flag color
    /// </summary>
    public Image playerColor;

    /// <summary>
    /// flag colors
    /// </summary>
    public Color[] playerFlag;

    /// <summary>
    /// update data
    /// </summary>
	public void UpdateData (PlayerType type, int srNo, int score)
    {
        scoreText.text = score + "";
        if (type == PlayerType.LeftPlayer)
        {
            playerColor.color = playerFlag[0];
            playerName.text = "Left player";
        }
        else
        {
            playerColor.color = playerFlag[1];
            playerName.text = "Right player";
        }

        serialNo.text = srNo + ".";
    }
}
