﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour {

    /// <summary>
    /// instance of game over UI
    /// </summary>
    public static GameOverUI instance;

    /// <summary>
    /// game winner text
    /// </summary>
    public Text gameWinnerText;

    /// <summary>
    /// player colors
    /// </summary>
    public Color[] playerColor;

    /// <summary>
    /// player flags
    /// </summary>
    public GameObject playerFlag;

    /// <summary>
    /// winner score
    /// </summary>
    public Text winnerScore;

    /// <summary>
    /// contains scores
    /// </summary>
    public GameObject contentHandler;

    /// <summary>
    /// score object
    /// </summary>
    public GameObject scoreObj;

    /// <summary>
    /// level controller
    /// </summary>
    LevelController levelController;

    private void Start()
    {
        // remove all customers
        CustomerSpawner customerSpawner = GameObject.Find("CustomerSpawner").GetComponent<CustomerSpawner>();
        customerSpawner.RemoveAllCustomers();
    }

    /// <summary>
    /// update top socres
    /// </summary>
    public void UpdateTopScores ()
    {
        levelController = GameObject.Find("LevelController").GetComponent<LevelController>();
        for (int i = 0; i < levelController.playerScores.Count; i++)
        {
            // will not take more than 10 items
            if (i > 9)
                break;

            GameObject obj = Instantiate(scoreObj) as GameObject;
            obj.transform.SetParent(contentHandler.transform, false);
            obj.GetComponent<Score>().UpdateData(levelController.playerScores[i].type, i + 1, levelController.playerScores[i].score);
        }
    }

    /// <summary>
    /// restarts game
    /// </summary>
	public void RestartGame ()
    {
        Debug.Log("Replay/restart");
        LevelController levelController = GameObject.Find("LevelController").GetComponent<LevelController> ();
        levelController.ResetLevel();
        Destroy(gameObject);
    }

    /// <summary>
    /// game quit
    /// </summary>
    public void Quit ()
    {
        Application.Quit();
    }

    /// <summary>
    /// show winner
    /// </summary>
    public void ShowWinner (PlayerType type, int score, bool isTie = false)
    {
        winnerScore.text = "Score : " + score;
        if (isTie)
        {
            // game tie
            gameWinnerText.text = "Game Tie!";
            playerFlag.GetComponent<Image>().color = Color.grey;
        }
        else
        {
            if (type == PlayerType.LeftPlayer)
            {
                // blue color
                gameWinnerText.text = "Left player is winner!";
                playerFlag.GetComponent<Image>().color = playerColor[0];
            }
            else if (type == PlayerType.RightPlayer)
            {
                // red color
                gameWinnerText.text = "Right player is winner!";
                playerFlag.GetComponent<Image>().color = playerColor[1];
            }
        }
    }

    /// <summary>
    /// game over ui
    /// </summary>
    /// <returns></returns>
    public static GameOverUI ShowUI()
    {
        if (instance == null)
        {
            Time.timeScale = 0;
            GameObject obj = Instantiate(Resources.Load("Prefabs/UI/GameOver")) as GameObject;

            GameObject canvas = GameObject.FindWithTag("MainCanvas");
            obj.transform.SetParent(canvas.transform, false);
            obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, obj.transform.localPosition.y, -10f);

            instance = obj.GetComponent<GameOverUI>();

        }
        return instance;
    }
}
