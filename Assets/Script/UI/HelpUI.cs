﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpUI : MonoBehaviour {

    /// <summary>
    /// instance
    /// </summary>
    public static HelpUI instance;

    /// <summary>
    /// On ok button click
    /// </summary>
    public void OnButtonClick ()
    {
        Time.timeScale = 1f;
        Destroy(gameObject);
    }

    /// <summary>
    /// Instantiates UI
    /// </summary>
    /// <returns></returns>
    public static HelpUI ShowUI()
    {
        if (instance == null)
        {
            Time.timeScale = 0;
            GameObject obj = Instantiate(Resources.Load("Prefabs/UI/Help")) as GameObject;

            GameObject canvas = GameObject.FindWithTag("MainCanvas");
            obj.transform.SetParent(canvas.transform, false);
            obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, obj.transform.localPosition.y, -10f);

            instance = obj.GetComponent<HelpUI>();
        }
        return instance;
    }
}
