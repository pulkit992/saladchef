﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    /// <summary>
    /// turning speed while player moving
    /// </summary>
    public float movingTurnSpeed = 360f;

    /// <summary>
    /// turning speed while player not moving
    /// </summary>
    public float stationaryTurnSpeed = 180f;

    /// <summary>
    /// move speed multiplier helps to increase speed
    /// while using reward given by customes-speed pickup
    /// </summary>
    public float moveSpeedMultiplier = 1f;

    /// <summary>
    /// rigidbody of player
    /// </summary>
    Rigidbody playerRigidbody;

    /// <summary>
    /// player's turn amount
    /// while rotating
    /// </summary>
    float turnAmount;

    /// <summary>
    /// player's forward amount
    /// </summary>
    float forwardAmount;

    /// <summary>
    /// list of salad/vegetable player have
    /// </summary>
    public List<Salad> salad;

    /// <summary>
    /// defines which table is at front of player
    /// </summary>
    Table currentTable;

    /// <summary>
    /// images of items
    /// which player has
    /// </summary>
    public Image[] Item;

    /// <summary>
    /// warning text/ info text for player
    /// </summary>
    public Text playerText;

    /// <summary>
    /// showing if player has any item combo
    /// </summary>
    public GameObject[] ItemCombo;

    /// <summary>
    /// blank/transparent sprite
    /// </summary>
    public Sprite emptySprite;

    /// <summary>
    /// instance of level controller class
    /// </summary>
    LevelController levelController;

    /// <summary>
    /// handles that player will move or not
    /// </summary>
    public bool canPlayerMove = true;

    /// <summary>
    /// awardOfPlayer
    /// </summary>
    Award award;

    /// <summary>
    /// speed awarded
    /// </summary>
    bool speedIncreased = false;

    /// <summary>
    /// speed bar container
    /// </summary>
    public GameObject speedBarContainer;

    /// <summary>
    /// awarded speed bar
    /// </summary>
    public Image speedBar;

    /// <summary>
    /// timer which shows the 
    /// total time of speed up
    /// </summary>
    float speedUpTime;

    /// <summary>
    /// remaining time for speed up
    /// </summary>
    float remainingSpeedtimer;

    void Start()
    {
        // speed bar should not show at the start of game
        speedBarContainer.SetActive(false);

        // finding levelcontroller script
        levelController = GameObject.Find("LevelController").GetComponent<LevelController>();

        // speed up time for player
        speedUpTime = levelController.speedUptime;

        // finding rigidbody of player
        playerRigidbody = GetComponent<Rigidbody>();

        // player warning/info should be blank at start of game
        playerText.text = "";

        // combo object should be disable
        // at starting of game
        ItemCombo[0].SetActive(false);
        ItemCombo[1].SetActive(false);

    }

    private void Update()
    {
        if (speedIncreased)
        {
            if (remainingSpeedtimer > 0)
            {
                remainingSpeedtimer -= Time.deltaTime;
            }
            else
            {
                speedIncreased = false;
                speedBarContainer.SetActive(false);
                GetComponent<PlayerUserController>().speedFactor = 1f;
            }
            speedBar.fillAmount = remainingSpeedtimer / speedUpTime;
        }
    }

    /// <summary>
    /// player movement
    /// </summary>
    /// <param name="move"></param>
    public void HandleMovement(Vector3 move)
    {
        Vector3 movement = move;
        if (move.magnitude > 1f)
            move.Normalize();

        move = transform.InverseTransformDirection(move);
        move = Vector3.ProjectOnPlane(move, Vector3.up);

        // turn amount and forward amount to head in desired direction
        turnAmount = Mathf.Atan2(move.x, move.z);
        forwardAmount = move.z;

        // check that can player move or not
        if (canPlayerMove && !LevelController.isGameOver)
        { 
            // rotating player into desired direction
            PlayerRotation();

            // adding velocity to player's rigidbody for moving
            PlayerMove(movement);
        }
    }

    /// <summary>
    /// rotates player on user input
    /// </summary>
    void PlayerRotation ()
    {
        // turnSpeed depends on player's forward movement
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);

        // rotating player
        transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
    }

    public void PlayerMove(Vector3 move)
    {
        Vector3 v = (move * 0.1f * moveSpeedMultiplier) / Time.deltaTime;
        
        // height should not affect by user's input
        // so taking current velocity of y axis
        v.y = playerRigidbody.velocity.y;

        //applying velocity to player's rigidbody
        playerRigidbody.velocity = v;
    }

    /// <summary>
    /// checking if player is nearby to any object
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Award")
        {
            if (col.GetComponent<Award> ().playerType == GetComponent<PlayerUserController> ().playerType)
            {
                award = col.GetComponent<Award>();
                playerText.text = "Award!";
            }
        }
        else
        {
            Table table = col.GetComponent<Table>();

            if (table.tableType == TableType.Chopper)
            {
                table.gameObject.GetComponent<Chopper>().playerController = this;
            }
            table.HighlightTable();
            currentTable = table;
        }
    }

    /// <summary>
    /// checking if player was nearby of any object and now going
    /// away then trigger exit calls
    /// </summary>
    /// <param name="col"></param>
    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Award")
        {
            award = null;
            playerText.text = "";
        }
        else
        {
            Table table = col.GetComponent<Table>();

            if (table.tableType == TableType.Chopper)
            {
                table.gameObject.GetComponent<Chopper>().playerController = null;
            }
            table.NormalTable();
            currentTable = null;
        }
    }

    /// <summary>
    /// resets player
    /// </summary>
    public void ResetPlayer ()
    {
        ClearWarningText();
        salad.Clear();

        // remove vegetables from player's HUD
        Item[0].sprite = emptySprite;
        Item[1].sprite = emptySprite;

        // salad text remove
        ItemCombo[0].SetActive(false);
        ItemCombo[1].SetActive(false);

        // remove speed bar if visible
        speedBarContainer.SetActive(false);
        speedIncreased = false;
    }

    /// <summary>
    /// clears the player's warning/info text
    /// </summary>
    void ClearWarningText ()
    {
        playerText.text = "";
    }

    /// <summary>
    /// pick vegetable/combination
    /// </summary>
    public void Pick ()
    {
        if (currentTable != null)
        {
            if (currentTable.isPickable)
            {
                // pick object
                if (salad.Count < 2)
                {
                    Salad _salad = new Salad();
                    int index = 0;

                    switch (currentTable.tableType)
                    {
                        case TableType.VegetableTable:
                            _salad.type = currentTable.vegetableType;
                            salad.Add(_salad);
                            index = levelController.GetVegeTableIndex(_salad.type);
                            Item[salad.Count - 1].sprite = levelController.vegetableData[index].vegetableImage;
                            break;

                        case TableType.Chopper:
                            Chopper chopper = currentTable.gameObject.GetComponent<Chopper>();
                            if (chopper.HasAnyCombination())
                            {
                                Debug.Log("chopper has combination! picking it up.");
                                _salad.combination = chopper.ChoppedSalad();
                                Debug.Log("salad combo count" + _salad.combination.Count);
                                _salad.isCombination = true;
                                salad.Add(_salad);
                                Item[salad.Count - 1].sprite = emptySprite;
                                ItemCombo[salad.Count - 1].SetActive(true);
                            }
                            else
                            {
                                Debug.Log("chopper dont have any combination.");
                                playerText.text = "Chopper is empty!";
                                Invoke("ClearWarningText", 1.2f);
                            }
                            break;

                        case TableType.Plate:
                            Plate plate = currentTable.gameObject.GetComponent<Plate>();
                            if (plate.HasAnyVegetable())
                            {
                                Debug.Log("Picking plate item");
                                _salad.type = plate.RemoveVegetable();
                                salad.Add(_salad);
                                index = levelController.GetVegeTableIndex(_salad.type);
                                Item[salad.Count - 1].sprite = levelController.vegetableData[index].vegetableImage;
                            }
                            else
                            {
                                Debug.Log("plate is empty");
                                playerText.text = "Plate is empty!";
                                Invoke("ClearWarningText", 1.2f);
                            }
                            break;
                    }  
                }
                else
                {
                    Debug.Log("can't pick up more objects");
                    playerText.text = "Hands are full!";
                    Invoke("ClearWarningText", 1.2f);
                }
            }
            else
            {
                Debug.Log("can't pick");
                playerText.text = "Can't Pick!";
                Invoke("ClearWarningText", 1.2f);
            }
        }
        else if(award != null)
        {
            Debug.Log("award picked up - " + award.awardType);
            PlayerUserController userController = GetComponent<PlayerUserController>();

            switch (award.awardType)
            {
                case AwardType.Score:
                    playerText.text = "Score Awarded!";

                    if (userController.playerType == PlayerType.LeftPlayer)
                    {
                        levelController.leftPlayerScore += levelController.playerScoreAward;
                        HeaderUI.instance.leftPlayerScore.text = "" + levelController.leftPlayerScore;
                        levelController.leftPlayerAwardCounter -= 1;
                        HeaderUI.instance.UpdateLeftPlayerAward(levelController.leftPlayerAwardCounter);
                    }
                    else if (userController.playerType == PlayerType.RightPlayer)
                    {
                        levelController.rightPlayerScore += levelController.playerScoreAward;
                        HeaderUI.instance.rightPlayerScore.text = "" + levelController.rightPlayerScore;
                        levelController.rightPlayerAwardCounter -= 1;
                        HeaderUI.instance.UpdateRightPlayerAward(levelController.rightPlayerAwardCounter);
                    }
                    break;

                case AwardType.Speed:
                    playerText.text = "Speed Awarded!";
                    speedBarContainer.SetActive(true);
                    speedIncreased = true;
                    remainingSpeedtimer = speedUpTime;
                    userController.speedFactor = 2f;

                    if (userController.playerType == PlayerType.LeftPlayer)
                    {
                        levelController.leftPlayerAwardCounter -= 1;
                        HeaderUI.instance.UpdateLeftPlayerAward(levelController.leftPlayerAwardCounter);
                    }
                    else if (userController.playerType == PlayerType.RightPlayer)
                    {
                        levelController.rightPlayerAwardCounter -= 1;
                        HeaderUI.instance.UpdateRightPlayerAward(levelController.rightPlayerAwardCounter);
                    }
                    break;

                case AwardType.Time:
                    playerText.text = "Time Awarded!";
                    
                    if (userController.playerType == PlayerType.LeftPlayer)
                    {
                        levelController.leftPlayerTimer += levelController.playerTimeAward;
                        levelController.leftPlayerAwardCounter -= 1;
                        HeaderUI.instance.UpdateLeftPlayerAward(levelController.leftPlayerAwardCounter);
                    }
                    else if (userController.playerType == PlayerType.RightPlayer)
                    {
                        levelController.rightPlayerTimer += levelController.playerTimeAward;
                        levelController.rightPlayerAwardCounter -= 1;
                        HeaderUI.instance.UpdateRightPlayerAward(levelController.rightPlayerAwardCounter);
                    }
                    break;
            }
            Invoke("ClearWarningText", 1.2f);
            Destroy(award.gameObject);
        }
    }

    /// <summary>
    /// drop vegetable/combination
    /// </summary>
    public void Drop ()
    {
        if (currentTable != null)
        {
            if (currentTable.isDropable)
            {
                //drop object
                if (salad.Count > 0)
                {
                    int index = 0;
                    switch (currentTable.tableType)
                    {
                        case TableType.VegetableTable:
                            if (currentTable.vegetableType == salad[0].type)
                            {
                                if (salad.Count == 1)
                                {
                                    Item[0].sprite = emptySprite;
                                }
                                else if (salad.Count == 2)
                                {
                                    Item[0].sprite = Item[1].sprite;
                                    Item[1].sprite = emptySprite;
                                }

                                // returning vegetable to its place
                                salad.RemoveAt(0);
                            }
                            else
                            {
                                Debug.Log("can't drop");
                                playerText.text = "Can't drop!";
                                Invoke("ClearWarningText", 1.2f);
                            }
                            break;

                        case TableType.Chopper:
                            Chopper chopper = currentTable.gameObject.GetComponent<Chopper>();
                            if (!chopper.IsChopperFull())
                            {
                                bool hasSalad = false;
                                for (int i = 0; i < salad.Count; i++)
                                {
                                    if (salad[i].isCombination)
                                        hasSalad = true;
                                }

                                if (!hasSalad)
                                {
                                    chopper.DropVegForChop(salad[0].type);

                                    if (salad.Count == 1)
                                    {
                                        Item[0].sprite = emptySprite;
                                    }
                                    else if (salad.Count == 2)
                                    {
                                        Item[0].sprite = Item[1].sprite;
                                        Item[1].sprite = emptySprite;
                                    }

                                    salad.RemoveAt(0);
                                }
                                else
                                {
                                    Debug.Log("first drop salad!");
                                    playerText.text = "Drop Salad first.";
                                    Invoke("ClearWarningText", 1.2f);
                                }
                            }
                            else
                            {
                                Debug.Log("chopper is full");
                                playerText.text = "Chopper is full!";
                                Invoke("ClearWarningText", 1.2f);
                            }
                            break;

                        case TableType.Dustbin:
                            Dustbin dustbin = currentTable.gameObject.GetComponent<Dustbin>();
                            int combinationIndex = -1;
                            for (int i = 0; i < salad.Count; i++)
                            {
                                if (salad[i].isCombination)
                                    combinationIndex = i;
                            }
                            
                            if (combinationIndex != -1)
                            {
                                ItemCombo[combinationIndex].SetActive(false);
                                
                                if (salad.Count == 2 && combinationIndex == 0)
                                {
                                    Item[0].sprite = Item[1].sprite;
                                    Item[1].sprite = emptySprite;
                                }
                                salad.RemoveAt(combinationIndex);
                                dustbin.TrashVegetable(GetComponent<PlayerUserController> ().playerType);
                            }
                            else
                            {
                                playerText.text = "Don't have salad!";
                                Invoke("ClearWarningText", 1.2f);
                            }
                            
                            break;

                        case TableType.Plate:
                            Plate plate = currentTable.gameObject.GetComponent<Plate>();
                            if (!salad[0].isCombination)
                            {
                                if (!plate.HasAnyVegetable())
                                {
                                    plate.CarryVegetable(salad[0]);
                                    if (salad.Count == 1)
                                    {
                                        Item[0].sprite = emptySprite;
                                    }
                                    else if (salad.Count == 2)
                                    {
                                        Item[0].sprite = Item[1].sprite;
                                        Item[1].sprite = emptySprite;
                                    }

                                    salad.RemoveAt(0);
                                }
                                else
                                {
                                    Debug.Log("Plate already has vegetable");
                                    playerText.text = "Plate is full!";
                                    Invoke("ClearWarningText", 1.2f);
                                }
                            }
                            else
                            {
                                Debug.Log("salad comnination can't drop on plate!");
                                playerText.text = "Salad can't drop here!";
                                Invoke("ClearWarningText", 1.2f);
                            }
                            break;

                        case TableType.CustomerTable:
                            CustomerCounter customerCounter = currentTable.gameObject.GetComponent<CustomerCounter>();
                            index = 0;
                            bool hasCombination = false;
                            for (int i = 0; i < salad.Count; i++)
                            {
                                if (salad[i].isCombination)
                                {
                                    index = i;
                                    hasCombination = true;
                                }
                            }
                                if (hasCombination)
                                {
                                    Salad _salad = salad[index];
                                    if (!customerCounter.isCounterEmpty)
                                    {
                                        ItemCombo[index].SetActive(false);
                                        if (salad.Count == 2 && index == 0)
                                        {
                                            Item[0].sprite = Item[1].sprite;
                                            Item[1].sprite = emptySprite;
                                        }
                                        salad.RemoveAt(index);

                                        customerCounter.GetCombination(_salad, this);
                                    }
                                    else
                                    {
                                        Debug.Log("no customer at this counter!");
                                        playerText.text = "Dont have customer";
                                        Invoke("ClearWarningText", 1.2f);
                                    }
                                }
                                else
                                {
                                    Debug.Log("player dont have any combination to drop");
                                    playerText.text = "Don't have Salad!";
                                    Invoke("ClearWarningText", 1.2f);
                                }
                            
                            break;
                    }
                }
                else
                {
                    Debug.Log("Nothing to drop");
                    playerText.text = "Nothing to drop!";
                    Invoke("ClearWarningText", 1.2f);
                }
            }
            else
            {
                Debug.Log("can't drop");
                playerText.text = "Can't drop!";
                Invoke("ClearWarningText", 1.2f);
            }
        }
    }
}
