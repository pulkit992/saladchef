﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// type of award
/// </summary>
public enum AwardType
{
    Speed,
    Time,
    Score
}

public class Award : MonoBehaviour {

    /// <summary>
    /// player type, who can pick up 
    /// this award
    /// </summary>
    public PlayerType playerType;

    /// <summary>
    /// award type - speed, time, score
    /// </summary>
    public AwardType awardType;

	// Use this for initialization
	void Start () {
		
	}
}
