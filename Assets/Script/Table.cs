﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// defines table type
/// </summary>
public enum TableType
{
    CustomerTable,
    Chopper,
    Dustbin,
    Plate,
    VegetableTable
}

/// <summary>
/// defines vegetable type
/// </summary>
public enum VegetableType
{
    Empty,
    Combination,
    Cucumber,
    Carrot,
    Tomato,
    Onion,
    Radish,
    Jicama
}

/// <summary>
/// defines vegetable of combination
/// type
/// </summary>
[System.Serializable]
public class Salad
{
    public VegetableType type;
    public List<VegetableType> combination;
    public bool isCombination = false;
}


public class Table : MonoBehaviour {

    /// <summary>
    /// highlight table material
    /// when player is nearby of it
    /// </summary>
    public Material highlightMaterial;

    /// <summary>
    /// normal material of table 
    /// </summary>
    public Material normalMaterial;

    /// <summary>
    /// table type
    /// </summary>
    public TableType tableType;

    /// <summary>
    /// defines the type of vegetable
    /// </summary>
    public VegetableType vegetableType;

    /// <summary>
    /// checking if table is pickable type
    /// </summary>
    public bool isPickable;

    /// <summary>
    /// checking if table is dropable type
    /// </summary>
    public bool isDropable;

    /// <summary>
    /// highlights the table to show user
    /// that player is nearby of this object
    /// </summary>
    public void HighlightTable()
    {
        GetComponent<MeshRenderer>().material = highlightMaterial;
    }

    /// <summary>
    /// normal table color
    /// </summary>
    public void NormalTable()
    {
        GetComponent<MeshRenderer>().material = normalMaterial;
    }
}
