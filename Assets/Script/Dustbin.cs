﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dustbin : MonoBehaviour {

    LevelController levelController;

    private void Start()
    {
        levelController = GameObject.Find("LevelController").GetComponent<LevelController>();
    }

    /// <summary>
    /// trash combo
    /// </summary>
    public void TrashVegetable(PlayerType type)
    {
        Debug.Log("trash");
        if (type == PlayerType.LeftPlayer)
        {
            // left player
            levelController.leftPlayerScore -= levelController.scoreDeductionRate;
            HeaderUI.instance.leftPlayerScore.text = "" + levelController.leftPlayerScore;
        }
        else
        {
            // right player
            levelController.rightPlayerScore -= levelController.scoreDeductionRate;
            HeaderUI.instance.rightPlayerScore.text = "" + levelController.rightPlayerScore;
        }
    }
}
