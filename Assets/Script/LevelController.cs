﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// class which contains all vegetable
/// name type and images
/// </summary>
[System.Serializable]
public class VegetableData
{
    public Sprite vegetableImage;
    public string vegetableName;
    public VegetableType type;
}

/// <summary>
/// contains the player score and player type
/// </summary>
[System.Serializable]
public class PlayerScores
{
    public int score;
    public PlayerType type;
}

public class LevelController : MonoBehaviour {

    /// <summary>
    /// contains players score in list
    /// </summary>
    public List<PlayerScores> playerScores;

    /// <summary>
    /// has a list of all vegetable
    /// name and images
    /// </summary>
    [Tooltip("Contains all combinations of salad which customer holds/demands")]
    public List<VegetableData> vegetableData;

    /// <summary>
    /// speed up time for player
    /// </summary>
    [Tooltip("Speed up time when player gets award.")]
    public float speedUptime;

    /// <summary>
    /// award
    /// </summary>
    [Tooltip("Reference for the award object")]
    public GameObject awardObj;

    /// <summary>
    /// time for player at the start of game
    /// </summary>
    [Tooltip("Initial time for player. when game starts")]
    public float initialPlayerTime;

    /// <summary>
    /// left player timer
    /// </summary>
    [HideInInspector]
    public float leftPlayerTimer;

    /// <summary>
    /// right player timer
    /// </summary>
    [HideInInspector]
    public float rightPlayerTimer;

    /// <summary>
    /// left player score
    /// </summary>
    [HideInInspector]
    public int leftPlayerScore;

    /// <summary>
    /// right player score
    /// </summary>
    [HideInInspector]
    public int rightPlayerScore;

    /// <summary>
    /// flag for game over
    /// </summary>
    public static bool isGameOver = false;

    /// <summary>
    /// left player controller
    /// </summary>
    [Tooltip("Reference for the left player controller")]
    public PlayerController leftPlayerController;

    /// <summary>
    /// right player controller
    /// </summary>
    [Tooltip("Reference for the right player controller")]
    public PlayerController rightPlayerController;

    /// <summary>
    /// awarded timer for player
    /// </summary>
    [Tooltip("Time award value for player")]
    public float playerTimeAward;

    /// <summary>
    /// awarded score for player
    /// </summary>
    [Tooltip("Score award value for player")]
    public int playerScoreAward;

    /// <summary>
    /// left player award counter
    /// </summary>
    [HideInInspector]
    public int leftPlayerAwardCounter;

    /// <summary>
    /// right player award counter
    /// </summary>
    [HideInInspector]
    public int rightPlayerAwardCounter;

    /// <summary>
    /// score for each salad item
    /// it multiplys with no of item 
    /// in the salad
    /// </summary>
    [Tooltip("Score value for each vegetable.\nit multiplys with the content in the salad and then adds score to player")]
    public int scoreForEachSaladItem;

    /// <summary>
    /// score deduction in case of 
    /// dissatisfied/angry customer
    /// </summary
    [Tooltip("Score duduction rate in case of angry/dissatisfied customer")]
    public int scoreDeductionRate;

    /// <summary>
    /// chopper in the restaurant
    /// </summary>
    [Tooltip("Chopper reference in restaurant")]
    public Chopper[] choppers;

    /// <summary>
    /// plates in the restaurant
    /// </summary>
    [Tooltip("Plate reference in restaurant")]
    public Plate[] plates;

    private void Start()
    {
        ReadScoreFile();
        InitializeGame();
    }

    /// <summary>
    /// read score file
    /// </summary>
    public void ReadScoreFile ()
    {
        playerScores = new List<PlayerScores>();

        TextAsset file = Resources.Load("File/Score", typeof(TextAsset)) as TextAsset;
        string[] lines = file.text.Split(new char[] { '\n' });

        for (int i = 0; i < lines.Length; i++)
        {
            if (string.IsNullOrEmpty(lines[i]))
                continue;

            PlayerScores _playerScores = new PlayerScores();

            string[] parts = lines[i].Split(new char[] { '=' });
            if (parts != null)// && parts.Length == 4)
            {
                int _score = int.Parse(parts[0].Trim());
                int _type = int.Parse(parts[1].Trim());
                _playerScores.score = _score;
                if (_type == 0)
                    _playerScores.type = PlayerType.LeftPlayer;
                else
                    _playerScores.type = PlayerType.RightPlayer;

                playerScores.Add(_playerScores);
            }
        }
    }

    private void OnApplicationQuit()
    {
        Debug.Log("quiting");
        WriteScoreFile();
    }

    /// <summary>
    /// write socre file
    /// </summary>
    public void WriteScoreFile ()
    {
        string updatedScore = "";
        for (int i = 0; i < playerScores.Count; i++)
        {
            // will not write more than 10 data
            if (i > 9)
                break;
            int _score = playerScores[i].score;
            int _type;
            if (playerScores[i].type == PlayerType.LeftPlayer)
                _type = 0;
            else
                _type = 1;
            updatedScore += _score + " = " + _type  + "\n";
        }
        string path = Application.dataPath + "/Resources/File/Score.txt";
        Debug.Log(updatedScore);
        File.WriteAllText(path, updatedScore);
        Debug.Log("saved");
    }

    /// <summary>
    /// initialize game
    /// </summary>
    void InitializeGame()
    {
        // initializing starting timer for both players
        leftPlayerTimer = rightPlayerTimer = initialPlayerTime;

        HeaderUI.instance.leftPlayerTimer.text = Mathf.Round(leftPlayerTimer) + " s";
        HeaderUI.instance.rightPlayerTimer.text = Mathf.Round(rightPlayerTimer) + " s";

        // initial score for player
        leftPlayerScore = 0;
        rightPlayerScore = 0;
        HeaderUI.instance.leftPlayerScore.text = "" + leftPlayerScore;
        HeaderUI.instance.rightPlayerScore.text = "" + rightPlayerScore;

        HeaderUI.instance.rightPlayerAward.SetActive(false);
        HeaderUI.instance.leftPlayerAward.SetActive(false);

        // update score in header UI at starting
        UpdateLeftPlayerScore(0);
        UpdateRightPlayerScore(0);

        // award counter is zero at the starting
        leftPlayerAwardCounter = rightPlayerAwardCounter = 0;
    }

    /// <summary>
    /// update left player score
    /// </summary>
    /// <param name="score"></param>
    public void UpdateLeftPlayerScore (int score)
    {
        leftPlayerScore += score;
        HeaderUI.instance.leftPlayerScore.text = leftPlayerScore + "";
    }

    /// <summary>
    /// score for right player
    /// </summary>
    /// <param name="score"></param>
    public void UpdateRightPlayerScore(int score)
    {
        rightPlayerScore += score;
        HeaderUI.instance.rightPlayerScore.text = rightPlayerScore + "";
    }

    /// <summary>
    /// destroy awards
    /// </summary>
    void DestroyAwards ()
    {
        GameObject[] objs;
        objs = GameObject.FindGameObjectsWithTag("Award");
        foreach (GameObject obj in objs)
        {
            Destroy(obj);
        }
    }

    private void Update()
    {
        if (leftPlayerTimer > 0)
        {
            leftPlayerTimer -= Time.deltaTime;
        }
        else
        {
            leftPlayerController.canPlayerMove = false;
        }

        if (rightPlayerTimer > 0)
        {
            rightPlayerTimer -= Time.deltaTime;
        }
        else
        {
            rightPlayerController.canPlayerMove = false;
        }

        HeaderUI.instance.leftPlayerTimer.text = Mathf.Round(leftPlayerTimer) + " s";
        HeaderUI.instance.rightPlayerTimer.text = Mathf.Round(rightPlayerTimer) + " s";

        if (Mathf.Round (leftPlayerTimer) == 0 && Mathf.Round (rightPlayerTimer) == 0 && !isGameOver)
        {
            //game over
            Debug.Log("left score " + leftPlayerScore + ",right score " + rightPlayerScore);
            isGameOver = true;
            GameOverUI.ShowUI();

            bool isTie = false;
            PlayerScores _playerScore = new PlayerScores();
            if (leftPlayerScore > rightPlayerScore)
            {
                // left player is winner
                GameOverUI.instance.ShowWinner(PlayerType.LeftPlayer, leftPlayerScore, isTie);
                _playerScore.score = leftPlayerScore;
                _playerScore.type = PlayerType.LeftPlayer;
                bool isAddedInList = false;
                List<PlayerScores> _Scores = new List<PlayerScores>();

                for (int i = 0; i < playerScores.Count; i++)
                {
                    _Scores.Add(playerScores [i]);
                }
                playerScores.Clear();

                for (int i = 0; i < _Scores.Count; i++)
                {
                    if (_Scores [i].score < leftPlayerScore && !isAddedInList)
                    {
                        playerScores.Add(_playerScore);
                        isAddedInList = true;
                    }
                    playerScores.Add(_Scores [i]);
                }
                if (!isAddedInList)
                    playerScores.Add(_playerScore);
            }
            else if (leftPlayerScore < rightPlayerScore)
            {
                // right player is winner
                GameOverUI.instance.ShowWinner(PlayerType.RightPlayer, rightPlayerScore, isTie);
                _playerScore.score = rightPlayerScore;
                _playerScore.type = PlayerType.RightPlayer;
                bool isAddedInList = false;
                List<PlayerScores> _Scores = new List<PlayerScores>();

                for (int i = 0; i < playerScores.Count; i++)
                {
                    _Scores.Add(playerScores[i]);
                }
                playerScores.Clear();

                for (int i = 0; i < _Scores.Count; i++)
                {
                    if (_Scores[i].score < rightPlayerScore && !isAddedInList)
                    {
                        playerScores.Add(_playerScore);
                        isAddedInList = true;
                    }
                    playerScores.Add(_Scores[i]);
                }
                if (!isAddedInList)
                    playerScores.Add(_playerScore);
            }
            else
            {
                // no one is winner
                isTie = true;
                GameOverUI.instance.ShowWinner(PlayerType.RightPlayer, rightPlayerScore, isTie);
            }

            if (GameOverUI.instance != null)
                GameOverUI.instance.UpdateTopScores();
        }
    }

    /// <summary>
    /// resets level
    /// </summary>
    public void ResetLevel ()
    {
        // initializing gmae
        InitializeGame();
        // game is not over
        isGameOver = false;

        // player can move
        leftPlayerController.canPlayerMove = true;
        rightPlayerController.canPlayerMove = true;

        // reset player positions
        leftPlayerController.transform.position = new Vector3(-1.5f, 0.113f, -1f);
        rightPlayerController.transform.position = new Vector3(1.5f, 0.113f, -1f);

        leftPlayerController.transform.rotation = Quaternion.identity;
        rightPlayerController.transform.rotation = Quaternion.identity;

        // resets player data
        leftPlayerController.ResetPlayer();
        rightPlayerController.ResetPlayer();

        // initializing customers again
        CustomerSpawner customerSpawner = GameObject.Find("CustomerSpawner").GetComponent<CustomerSpawner>();
        StartCoroutine(InitializeCustomers (customerSpawner));

        // destroy awards if any
        DestroyAwards ();

        // clear choppers 
        for (int i = 0; i < choppers.Length; i++)
        {
            choppers[i].ResetChopper();
        }

        // clear plates
        for (int i = 0; i < plates.Length; i++)
        {
            plates[i].ResetPlate();
        }

        Time.timeScale = 1f;
    }

    IEnumerator InitializeCustomers (CustomerSpawner spawner)
    {
        Debug.Log("waiting for 1 sec");
        yield return new WaitForSeconds(0.5f);
        spawner.ResetTable();
        yield return new WaitForSeconds(0.5f);
        Debug.Log("initialize custoemrs");
        spawner.InitializeCustoemrs();
    }

    /// <summary>
    /// returns the index of a required
    /// vegetable type
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
	public int GetVegeTableIndex (VegetableType type)
    {
        int index = 0;
        for (int i = 0; i < vegetableData.Count; i++)
        {
            if (type == vegetableData[i].type)
            {
                index = i;
            }
        }
        return index;
    }

    /// <summary>
    /// spawn award at random place in level
    /// </summary>
    public void SpawnAward (PlayerType type)
    {
        Vector3 pos = new Vector3(Random.Range (-2.7f, 2.7f), 0.3f, Random.Range (-2.4f, 1.2f));
        Quaternion angle = Quaternion.Euler(0f, Random.Range (0f, 355f), 0f);
        GameObject obj = Instantiate(awardObj, pos, angle);
        Award award = obj.GetComponent<Award>();

        award.playerType = type;
        award.awardType = (AwardType)Random.Range (0,3);

        if (type == leftPlayerController.GetComponent<PlayerUserController> ().playerType)
        {
            // left player
            leftPlayerAwardCounter += 1;
            HeaderUI.instance.UpdateLeftPlayerAward(leftPlayerAwardCounter);
        }
        else if (type == rightPlayerController.GetComponent<PlayerUserController>().playerType)
        {
            // right player
            rightPlayerAwardCounter += 1;
            HeaderUI.instance.UpdateRightPlayerAward(rightPlayerAwardCounter);
        }
    }
}
