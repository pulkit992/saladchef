﻿using UnityEngine;

/// <summary>
/// defines player type
/// left player or right player
/// </summary>
public enum PlayerType
{
    LeftPlayer,
    RightPlayer
}

public class PlayerUserController : MonoBehaviour {

    /// <summary>
    /// player controller
    /// </summary>
    PlayerController playerController; 

    /// <summary>
    /// camera 
    /// </summary>
    Transform cam;             
    
    /// <summary>
    /// contains player inputs for movement
    /// </summary>
    private Vector3 move;

    /// <summary>
    /// defines player type
    /// left player or right player
    /// </summary>
    public PlayerType playerType;

    /// <summary>
    /// player UI which contains 
    /// vegetable, which is picked up by player
    /// </summary>
    public GameObject canvasObj;

    /// <summary>
    /// player movement speed factor
    /// </summary>
    public float speedFactor = 1f;

    private void Start()
    {
        // finding camera
        cam = Camera.main.transform;

        // finding player controller for player
        playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        // canvas always faces camera so that player always can
        // see UI of player. Dosent matter player rotates in any direction.
        canvasObj.transform.LookAt(cam);

        if (playerController.canPlayerMove)
        {
            // picking up vegetable/combination
            if (playerType == PlayerType.LeftPlayer && Input.GetKeyDown(KeyCode.Q))
            {
                playerController.Pick();
            }
            else if (playerType == PlayerType.RightPlayer && Input.GetKeyDown(KeyCode.O))
            {
                playerController.Pick();
            }

            // droping vegetable/combination
            if (playerType == PlayerType.LeftPlayer && Input.GetKeyDown(KeyCode.E))
            {
                playerController.Drop();
            }
            else if (playerType == PlayerType.RightPlayer && Input.GetKeyDown(KeyCode.P))
            {
                playerController.Drop();
            }
        }
    }
    
    private void FixedUpdate()
    {
        // variables for horizontal (h) and vertical (v) inputs
        float h = 0f;
        float v = 0f;

        // checking player type i.e. left player or right player
        // and on the basis of that taking player's input
        if (playerType == PlayerType.LeftPlayer)
        {
            h = Input.GetAxis("LeftPlayerHorizontal");
            v = Input.GetAxis("LeftPlayerVertical");
        }
        else if (playerType == PlayerType.RightPlayer)
        {
            h = Input.GetAxis("RightPlayerHorizontal");
            v = Input.GetAxis("RightPlayerVertical");
        }

        // assigning player's horizontal and vertical movement
        move = new Vector3(h, 0f, v) * speedFactor;

        // moving player
        playerController.HandleMovement(move);
    }
}
